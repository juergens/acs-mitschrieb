
# Load in new config settings
include _CONFIG.txt

# This combines all the filepaths in SECTIONS_FILEPATH file
SECTIONS := $(shell cat $(SECTIONS_FILEPATH) | tr '\n\r' ' ' | tr '\n' ' ' )

TEMPLATE_DIR=$(shell realpath $(shell dirname $(TEMPLATE)))

BASE_CMD=pandoc --toc --bibliography=$(REFERENCES)  --csl=../csl/$(CSL).csl --standalone --self-contained --toc -N  --bibliography=$(REFERENCES) --reference-links --reference-location section -V links-as-notes

WEB_CMD=$(BASE_CMD) --webtex --section-divs

TEX_CMD=TEXINPUTS=$(TEXINPUTS):$(TEMPLATE_DIR) $(BASE_CMD) --template=../$(TEMPLATE) --top-level-division=chapter --pdf-engine=xelatex

FILES =  $(shell find source/ -type f -name '*')

OUT=build/$(BUILDNAME)

.PHONY: clean
default: pdf
.DEFAULT_GOAL := pdf

all: pdf tex html mobi

clean:
	rm -rf build

# build the document
$(OUT).pdf: source $(FILES)
		mkdir -p build && cd ./source/  && \
		$(TEX_CMD) -o ../build/$(BUILDNAME).pdf $(SECTIONS)

pdf: $(OUT).pdf

$(OUT).tex: source $(FILES)
		mkdir -p build && cd ./source/  && \
		$(TEX_CMD) -o ../build/$(BUILDNAME).tex $(SECTIONS)

tex: $(OUT).tex

# build document as html
$(OUT).html: source $(FILES)
		mkdir -p build && cd ./source/ && \
		$(WEB_CMD) --reference-links -o ../build/$(BUILDNAME).html -t html --css ../$(HTML_STYLE) $(SECTIONS)

html: $(OUT).html

# build as ebook
$(OUT).fb2: source $(FILES) build
		mkdir -p build && cd ./source/ && \
		$(WEB_CMD) -o ../build/$(BUILDNAME).fb2 $(SECTIONS)

ebook: $(OUT).fb2

$(OUT).mobi: ebook
		cd ./source/ && \
		ebook-convert ../build/$(BUILDNAME).fb2 ../build/$(BUILDNAME).mobi > /dev/null

mobi: build/$(BUILDNAME).mobi


# the following are just personal convenience
reader: pdf
		nohup evince $(OUT).pdf &

loop: reader
		 while inotifywait -e close_write source; do make pdf; done

editor:
		nohup atom . &

ide: editor reader loop
