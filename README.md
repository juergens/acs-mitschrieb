
# Access Control Systems

Vorlesungmitschrieb von Björn Jürgens. Basierend auf der Vorlesung *Access Control Systems - Foundations and Practice* von *Prof. Dr. Hannes Hartenstein* im SS2018 am KIT.
Im wesentlichen ist das eine Liste der wichtigsten Stichwort, die in der Vorlesungen gefallen sind mit einer kurzen Erklärung.

You can find the compiled documents  [here](https://gitlab.com/juergens/acs-mitschrieb/builds/artifacts/master/browse/build/?job=build)

forked from https://gitlab.com/juergens/markdown-latex-boilerplate

## CI

if for some reason the CI-Runner stops working, you can set up a new one like this

get token from: Setting-->
CI / CD Settings --> Runners --> Specific Runners --> Setup a specific Runner manually

Configure Runner with token from above:

	docker run --rm -t -i -v /srv/gitlab-runner-acs/config:/etc/gitlab-runner --name gitlab-runner-acs gitlab/gitlab-runner register

start runner

	docker run -d --restart always -v /srv/gitlab-runner-acs/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock --name gitlab-runner-acs gitlab/gitlab-runner:latest

view runner's logs

	docker logs gitlab-runner

Damit das mobi verschickt wird, muss der tag protected sein, denn sonst kommen die Variablen nicht beim runner an.
