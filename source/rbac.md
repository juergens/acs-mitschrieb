
# Role-Based Access Control (RBAC)

lecture is based on @RBAC98

## introduction

### Motivation + goal

Motivation: many subject + many objects --> ACM is large

Obervation: many user have similar permissions

--> reduce complexity by assigning roles to users, and permissions to roles

### definitions

> A role based access control (RBAC) policy bases access control decisions on the functions a user is allowed to perform within an organization. The users cannot pass access permissions on to other users at their discretion.
>
> This is a fundamental difference between RBAC and DAC.

[D.F. Ferraiolo and D.R. Kuhn (1992)]

> Role: A job function within the organization that describes the authority and responsibility conferred on a user assigned to the role.

[@RBAC96]

> A permission is an approval of a particular mdoe of access to one or more objects in the system. [...] permissions are always positive [...]

[@RBAC98]

- Role vs Group:
	- group = collection of users
	- role = collection of users + collection of rights

## Security principles

Security principles that can be modeled by RBAC easily

- Least privilege
- Separation of duties (SoD)
	- cooperation of multiple users is required to complete sensitive tasks.
- Dual control
	- Special case of SoD
	- „demands the participation of at least two people in the completion of a single task.“
- Data abstraction
	- “Data abstraction is supported by means of abstract permissions, such as credit and debit for an account object, rather than the read, write, execute permissions typically provided by the operating system.”

### RBAC limitations

RBAC cannot control sequence of operations

## RBAC models

alle 3 Modelle kommen von  @RBAC96

### Base model ($RBAC_0$)

$$ RBAC = (U, R, P, S, PA, UA, user, roles) $$

- $U$: finite set of users
- $R$: finite set of roles
- $P$: finite set of permissions
- $S$: finite set of sessions
- $UA \subseteq U \times R$: user-role relation
- $PA \subseteq R \times P$: permission-role relation
- $user: S\to U$ (not the same _users_ as above) maps Session $s_i$ to user
- $roles: S \to 2^R$ (not the same _roles_ as above) maps session $s_i$ to role
	- $roles(s_i) \subseteq \{r\mid user(s_i,r)\in UA\}$
	- permission of session: $\cup_{r\in roles(s_i)} \{p \min (p,r)\in PA\}$


When user starts sesion she selects roles for this sessions.

Session does not have all roles of the user, to comply with principle of least privilege

![BASE RBAC](assets/markdown-img-paste-20180907144519372.png)

### Hierarchical model ($RBAC_0$)

- Based on Base model
- Idea: reflect structure of organization
- Hierarchies are partial orders
	- reflexive, transitive, antisymmetric
- definition
	- wie oben, aber:
	- $RH$ is partial order
	- $roles(s_i)$ hat auch die geerbten Rollen
	- Permissions hat auch die geerbten Permissions

![](assets/markdown-img-paste-20180907144932191.png)

### Constrained model ($RBAC_2$)

Based on Base model

> $RBAC_2$ is unchanged from $RBAC_0$ except for requiring that thee be a collection of contraints that determne whether or not values of various components of $R_0$ are acceptable. Only acceptable values will permitted

- most common constrain:
	- roles are mutually exclusive
	- roles have maximum number of members
- implements SoD
- implementations:
	- via "constraints": restrictions of state
	- via "prerequisite conditions":restrictions of state changes

## Administrative model of RBAC

- Defines who is allowed to change the RBAC state
- idea: use „administrative roles“ to assign administrative permissions

## conclusio

### Paper discussion

[@Schaad:2001:RAC:373256.373257]

todo...

### Role engineering and mining

- Role Engineering:
	- process of determining the role-configuration for RBAC system
	- roles, user-to-role relation, role-to-permission relation
- role mining
	- automated, data driven, buttom-up
-Role Mining Problem (RMP)
	- Given
		- User to Permission assignment (UPA)
	- Find
		- Number of Roles
		- User to Role assignment (UA)
		- Role to Permission assignment (RA)
	- So that
		- Number of Roles is minimal
		- UA and RA combined equal UPA

variants of RMP

- problem: RMP is too precise --> too many roles
- idea: add leeway
- $\delta$-approx RMP:
	- Given
		- User to Permission assignment (UPA)
		- relative error rate $\delta$
	- Find
		- Number of Roles
		- User to Role assignment (UA)
		- Role to Permission assignment (RA)
	- So that
		- Number of Roles is minimal
		- UA and RA combined equal UPA with error at most $\delta$

- idea: fix number of roles
- Minimal Noise RMP
	- Given
		- User to Permission assignment (UPA)
		- Number of Roles
	- Find
		- User to Role assignment (UA)
		- Role to Permission assignment (RA)
	- So that
		- Error rate $\delta$ is minimal
		- UA and RA combined equal UPA with error $\delta$

difficulty: RMP is NP-Hard

## Summary

- Role-based Access Control is one the most widely adopted AC models
- Compromise of simplicity and expressiveness
	- CCS classification of ACM TISSEC paper by Ahn & Sandhu: Programming Languages, Database Management
- Roles might already exist in an organization
	- But: do those roles map one-to-one to IT access rights?
- Otherwise: role engineering is required
- Challenges:
	- High number of roles --> Role Mining
- Administrative models can simply re-use concept of RBAC
- Many “Addons” to RBAC proposed
