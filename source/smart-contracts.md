
# Ethereum & Smart Contracts


## Smart Contracts

- executable code stored on a Blockchain

## Ethereum

- p2p + blockchain
- stronger scripting language than bitcoin



## Pitfalls

- Halting Problem
	- solution: gas
		- only spent limited amount of computing power
		- miner gets compensated, even if computation was not finished
		- conversion from gas to ether is part of transaction
- technical bug
	- transactions grow to big to fit into block
	- contract allow unauthorized access

## Summary

- Smart Contracts are...
	- Executable code on a distributed consensus system
	- Self-enforcing and irreversible
- Ethereum uses...
	- Public Key Cryptography to enforce AC on decentralized applications
	- A Peer-to-Peer Network to publish transactions and blocks
	- Blocks that aggregate transactions to ensure consistency and prevent double spends
	Proof-of-Work for publishing blocks to prevent Sybil attacks
	- A Turing-complete scripting language to specify AC policies and Smart Contracts
- Ethereum is...
	- not anonymous but pseudonymous
	- secure relative to economic assumptions
