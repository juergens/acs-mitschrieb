# Secure Data Sharing

secure data outsourcing, but with sharing.

## Challenge

-  Key management
	- encrypted data is public
	- keys are in lockboxes
	- keys to lockboxes are shared
-  Revocation
	- via re-encrypting lockboxes

combine lockboxes and assymetric keys for new features

## Shared Cryptographic File Systems
### SiRiUS

![sirius](assets/markdown-img-paste-20180915224005220.png)

![sirius with 2 user](assets/markdown-img-paste-20180915224024447.png)

- integrity of data and metadata is ensured via signatures and async keys
- freshness is not ensured
- meta file needs to be reencrypted after every change

### others

![comparison](assets/markdown-img-paste-20180915224351239.png)


## Summary

- Secure Data Sharing (SDS) enforces access control bycryptographic means
- SDS “complements” Secure Data Oursourcing with elements for clientside control and sharing functionality --> cryptographic enforcement of integrity, freshness
- Basic building blocks come from shared cryptographic file systems (SiRiUS) and are deployed (to some extend) in current services like Boxcryptor
- Revocation of rights can be resource-intensive (eager revocation); lazy revocation is less resource-intensive, but might not meet security requirements
- Active current research on performance of secure data sharing. Research on usability required as well (key management, group key management) -> Key graph as fundamental concept for analysis of secure data sharing
