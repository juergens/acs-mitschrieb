
# Attribute Based Access Control

- Problem with RBAC
	- static/inflexible
		- no short-term-roles
		- assigning roles requires bueaucratic/administrative process
	- ignores context of access request
- solution: Attribute Based Access Control (ABAC) is an access control system, in which access decision are made based on the possession of attributes rather than the identity of users
- [@ABAC14, pp. vii-8]


## ABAC
### Definition

> A logical access control methodology where authorization to perform a set of operations is determined by evaluating attributes associated with the subject, object, requested operations, and, in some cases, environment conditions against policy, rules, or relationships that describe the allowable operations for a given set of attributes.

> Attributes are characteristics that define specific aspects of the subject, object, environment conditions, and/or requested actions that are predefined and preassigned by an authority.

- policy decision point (PDP)
- policy enforcement point (PEP)
- Natural Language Policy (NLP)

> The purpose of logical access control is to protect objects [...] from unauthorized operations.

[@ABAC14]

### Model

elements:

- Elements
	- Authorized users (U)
	- User attributes (UA)
	- Operations (Op)
	- Objects (O)
	- Object attributes (OA)
	- Environmental attributes (EA)
- Relations
	- User to user attribute assignments (UUA)
	- Object to object attribute assignments (OOA)
- Reference mediation: Determines which users can perform which operations on which objects based on attributes assigned to the user, attributes assigned to the object, environmental conditions and either a set of rules and/or a set of relations
	- {grant, deny} ← decision(A(u) × A(o) × A(e) × Rules × op)
- $ABAC_rule$: if granted:
	- (u,op,o) is a privilege
	- (op,o) is capability for u
	- (u,op) is access control entry for o

![abac model from @ABAC14](assets/markdown-img-paste-20180912205920316.png)

### XACML

- eXtensible Access Control Markup Language
- general-purpose access control policy language



### Administrative Model


![Example of ACM Functional Points](assets/markdown-img-paste-20180912210228619.png)


![Enterprise ABAC Scenario Example](assets/markdown-img-paste-20180912210029417.png)

... it's complicated

- reference architecture (from @ABAC14)
	- Policy Decision Points (PDPs)
	- Policy Enforcement Points (PEP)
	- Policy Administration Points (PAPs)
	- Policy Information Points (PIPs)
- Another reference architecture is the Next Generation Access Control standard [ANSI499].


## Deployment
### Four phases to ABAC (NIST)

- Initiation
	- gather requirements
	- check capabilities
- Acquisistion
	- generate business process
	- buy/develop system
- Implementation
- Operations

(I don't see how this is different to any other deployment. I think it was only included in the paper for completness, since it aimed at business and not scientists)

## attribute assurance
### Trust relationships
### attribute assurance
## Delegation

> Delegation enables a user to temporarily and dynamically alter the design of an access control system after policies have been created to account for everyday changes that policies are insufficient to address.

- Consists of three access control elements
	- Delegator
	- Delegatee
	- Delegated access right

### Overview
### Challenges with the flexibility of ABAC
## Summary
