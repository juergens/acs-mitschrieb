
# Introduction
## organisationnal
### Course overview

![overview part 1](assets/markdown-img-paste-20180905110232716.png)

![overview part 2](assets/markdown-img-paste-20180905110256286.png)

## recap of ITSMan Lecture

### General Access Control Process

general steps:

1. user authenticates
2. system makes access  decision
3. user gets access to resource

![Typical Access Control Process](assets/markdown-img-paste-20180906102852141.png)

- vokabeln
	- _lattice_: Gitter



### Access Control Matrix

-  Access Control Matrix (ACM)
  - view on access rights
	- usually sparse
	- structure
		- columns: resources
		- rows: subjects (users, process, ...)
		- cell: rights
- Capability Lists
	- implementation of ACM where rows are stored
- Access Control Lists (ACL)
	- implementation of ACM where columns are stored
	- most common implementation

### Access Control Strategies & Models

![Access Control Strategies & Models](assets/markdown-img-paste-20180906103544765.png)

  - Discretionary Access Control (DAC)
	- Mandatory Access Control (MAC)
	- Role Based Access Control (RBAC)
	- Attribute Based Access Control (ABAC)

Administrative Model

- Who is allowed to manage access privileges?
	- internal (subjects are part of the model)
	- externel (e.g. admin, dev)
- Administrative RBAC (ARBAC)
	- RBAC mit Admin-Rolle, die ACM ändern darf

![system layers](assets/markdown-img-paste-20180906103924942.png)

- "Operating System" referes to an abstract system in the lecture:
	- “An operating system (OS) is system software that manages
computer hardware and software resources and provides common
services for computer programs.”
	- Processes acting on behalf of users are executed by the CPU,
referencing different resources
	- multi-user systems


### OM-AM framework

![OM AM](assets/markdown-img-paste-2018091111372953.png)

[great source]( https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&ved=2ahUKEwjQufCV07LdAhXMUlAKHbMRB5wQFjABegQIBRAC&url=https%3A%2F%2Fcsrc.nist.gov%2Fcsrc%2Fmedia%2Fpublications%2Fconference-paper%2F2000%2F10%2F19%2Fproceedings-of-the-23rd-nissc-2000%2Fdocuments%2Fpapers%2F313slide.pdf&usg=AOvVaw2ImI0Q5yXIta_lIDEChzsb)

a more detailed source is @OMAM00

- OM AM in General
	- Objectives (or _requirement_ or _policy_)
	- Model
	- Architecture
	- Mechanism (or _protkol_)
- Mandatory Access Control (MAC)
	- No information leakage
	- Lattices (Bell-LaPadula aka _MAC_)
	- Security kernel
	- Security labels
- Discretionary Access Control (DAC)
	- Owner-based discretion
	- numerous
	- numerous
	- ACLs, Capabilities, etc
- RBAC
	- Policy neutral
	- RBAC96
	- user-pull, server-pull, etc.
	- certificates, tickets, PACs, etc.


### Reference Monitor

![Reference Monitor](assets/markdown-img-paste-20180906104153672.png)

![Reference Monitor - extended](assets/markdown-img-paste-20180906104221346.png)

interface between OS and middle ware, but requrires implementation on hardware level

### security kernel

single most often used technique for building a highly secure operating

![security kernel](assets/markdown-img-paste-20180906104505251.png)

### Linux Security Modules (LSM)

- Reference Monitor Interface --> Hooks
- Reference Monitor Implementation --> Kernel Module
- Implementations: AppArmor, SELinux, ...

### Summary

- Definitions and recap of access control basics
- Administrative model defines how changes to access rights legitimately happen
- The Reference Monitor Concept constitutes a systems engineering perspective on Access Control, requiring implementations to be:
	1. Evaluable
	2. Always invoked
	3. Tamper-proof
- To implement a reference monitor, a Trusted Computing Base is needed. In case of OS-level access control this requires software primitives (e.g., LSM) as well as hardware support (e.g., protection rings). A common approach for OS-level access control implementations are security kernels, aiming at a reduction of the attack surface.
- Recent development: Intel SGX implements secure enclaves to run code in potentially malicious environments while providing integrity and confidentiality guarantees --> Lecture on Trusted Execution Environments
