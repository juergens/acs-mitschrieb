
# Blockchain & Bitcoin
### Public Verifiability

- all old transactions are public --> Blockchain + p2p
- all new transactions are public --> p2p
- transactions are signed cryptgraphically

### p2p

- regular p2p network
- flooding
	- new blocks
	- new transaction
- sharing of blockchain

### Bitcoin transactions

- public ledger stores transactions (and not explicit currency)
	- transaction graph
	- balance is sum of unspent transactions
- account (in traditional banking) is the set of private keys

### Blockchain

- data structure that can be used as a decentralized database
- public
- creates immutabilty of the past
- every block contains
	- items (e.g. list of transactions)
	- hash of last block

### Mining

- problem: blockchain secures the past, but what about the present?
- solution
	- proof-of-work to make sure no duplicate blocks are created
	- results are verified by peers
- process
	- all new transaction are published
	- miner create new block from set of transactions
	- miner try to solve puzzle
	- when solution, they publish the result as a new block
	- peers accept block (or not, if there is already a newer block, or there is some mistake)
	- if there are 2 blocks at the same time
		- all miners switch once there is yet another block
- result:
	- only very few canditates for new blocks circulate
	- it's easy to decide which one will get accepted


## Access Control in Bitcoin

- general AC policy
	- “Only the owner of the account is allowed to issue transactions from that account”
	- “The account must have the required balance”
- more sophisticated policies
	- via stack-based scripting language
	- example: payout when sha-collision is found


## Summary
- Bitcoin uses...
	- Public Key Cryptography to enforce AC on “money” a Peer-to-Peer Network to publish transactions and blocks
	- Blocks that aggregate transactions to ensure consistency and prevent double spends
	- Proof-of-Work for publishing blocks to prevent Sybil attacks a stack-based scripting language to specify AC policies
- Bitcoin is...
	- not anonymous but pseudonymous
	- secure relative to economic assumptions
- Access Control in Bitcoin
	- Transactions (Scripts)
	- Block generation (Proof-of-Work)
	- Money itself
