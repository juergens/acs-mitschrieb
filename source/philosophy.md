
# Philosophy & Values

> A lot of discussions on architecture and mechansims are actually discussions on objectives and values ... but done on the wrong layer.

Prof. Hannes Hartenstein

> Acess control is using computers to exercise power over humans

Björn Jürgens

- We need to define Values before we can define objectives
	- Why does someone need access?
	- Why can someoen give access?
- We can infer Values from objectives
- power disguised as AC
	- access to information is power
	- being able to restict people is power


![Basic Human Values (Schwartz, 1994)](assets/markdown-img-paste-20180912131409938.png)

## Value-Sensitive Design (VSD)

- aim: system design according to stakeholders values
- investigations (Friedman, 2008):
	1. **Conceptual**:
		- identification of stakeholders and their values,
		- clarification of terms (e.g., “trust”)
		- what values are affected?
	1. **Empirical**:
		- consideration of the human context of the system,
		- e.g.
			- ,stakeholders’ apprehension of individual values
			- “how do users prioritize conflicting values?”
	1. **Technical**:
		- effects of technical properties of the system on values

![](assets/markdown-img-paste-20180912132102669.png)

- Value: What's important for someone?
- Norm: Prescriptions/restrictions on action
- Design Requ.: technical property of system

![VSD vs OS-AM](assets/markdown-img-paste-20180912132240474.png)


### Decision Making

- input:
	- Values
	- Values scoring for potential design-decisions
- output:
	- optimal set of decisions
- solutions:
	- cost-benefit-analysis: convert values to money
	- direct trade-offs: compare options
	- maximize for most important value
	- inovate: improve design with technical innovation
	- satisficing: just go with any solution that satisfies requirements
	- ... more

### Value Scoring

![example: value - norm - design](assets/markdown-img-paste-20180912202228790.png)
