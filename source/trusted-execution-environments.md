# Trusted Execution Environments


## Motivation and Problem Statement

- motivation:
	- I don't trust my OS
	- I don't trust all my hardware
	- I only trust a very small part of my hardware


- common attacks
	- cold boot
	- bus probing
	- exchange trusted hardware with malicious hardware

goal reduce trusted computing base from "entire computer + OS" to "as smal as possible"

## Concepts


- Hardware Security Modules (HSM)
	- Collective for dedicated physical devices that support security-relevant and/or cryptographic functionalities
	- usually secret privatekey in procesor
		- temperproof
		- not probeable
- Trusted Platform Module (TPM)
	- by AMD, HP, Intel and Microsoft
	- a dedicated chip on motherboard
	- used to build a chain of trust
- Attestation
	- private key (from hardware) signs program code
	- is done remote via untrusted channel

## Technical Realisations

- solutions by
	- intel (SDX)
	- amd
	- arm

## TEEs in Research and Practice
## Summary


- TEEs use demonstrably compliant components
	- Goal: reduction of complexity and attack surfaces
Key components: memory protection for access control rules and hard- and - software with identities to determine access control subjects
- All current solutions are hardware-based
	- Hardware is most privileged
	- Every new layer increases code base and thus complexity
- Focus of solutions’ use cases differ
	- Dedicated hardware (HSM)
	- Support for reference monitors (ARM)
	- Cloud and Virtualization (TPM, AMD)
	- Isolation of user software (SGX)
- TEEs have the possibility to aid the creation of secure software and to reduce the number of trusted parties, but still the software provider is the unavoidable trust anchor
