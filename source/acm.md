
# Access control matrix

- underlying concept of all AC-Strategies
- formerly know as HRU model (Harrison, Ruzzo, Ullman)

- consists of
	- Subjects
	- Objects (resources)
	- Rights
	- Entries

- the ACM and answers to the Safetey Question come from @Harrison:1976:POS:360303.360333

## Safety Question

### Definitions

- Leak: Adding a right r where there was not one before
- Safe:
- Safety Question: "Does there exist one algorithm for determining whether an arbitrary protection system $S$ with initial state $s_0$ is safe with respect to a generic right $r$?"

- create subject $s$; create object $o$
	- Creates new column/row in ACM
- destroy subject $s$; destroy object $o$
	- Removes column/row from ACM
- enter $r$ into $A[s, o]$
	- Adds right $r$ for subject $s$ over object $o$
- delete $r$ from $A[s, o]$
	- Removes right $r$ for subject $s$ over object $o$

- command
	- Iff all conditions are met, execute all operations
	- only AND is allowed (OR can be simulated with mulitple commands)

			command <name>(<parameters>)
			if
				<conditions>
			then
				<operations>
			end

	- Mono-conditional: exactly 1 condition
	- Mono-operational: exactly 1 operation
- HRU-revisited: some definitions in the original paper are not 100%. An other papers clearified and called the result "HRU-revisited"

### proof

![](assets/markdown-img-paste-20180914132922111.png)

- einige Rechte repräsentieren Zustände (_X,Y_)
- Diagonal repräsentiert Band
- Jedes Subject _owns_ Rechten Nachbaren (dadurch wird Beziehung hergestellt und der Kopf kann zu dem Nachbarn bewegt werden und umgekehrt)
- Recht _q,p,..._ ist die Position auf Band und der aktuelle Zustand
- Jedes Command benötigt _q_
- Jedes Command kodiert möglichen Zustandsübergang

![Beispiel: Lese X im Zustand q, Schreibe Y, betrete Zustand_p,Kopf nach Links, ](assets/markdown-img-paste-20180914133230993.png)

### results

- Safety Question for Mono-Operational Commands
	- decidable
	- proof
		- Consider minimal sequence of commands to leak a right
		- Show that even in the worst case the sequence length is bounded by a polynomial
- Safety Question for general Commands
	- undecidable
	- because ACM simluates Touring machine

- without "create" it's in PSPACE
- monoconditional without destroy is decidable

## conclusion

### Discussion

- Do you consider the definition of the safety property and the system model appropriate?
- How does the undecidability result affect real world systems?
	- Real systems are always finite --> always decidable.
	- Complexity can still prevent decision in real systems.
- How does the undecidability result affect research?
	- Undecidability gives a very general limit of what is possible and what not.
	- Access control systems have to be limited (e.g., mono-operational) to be decidable.
- Mono-operational systems can be proven safe so why are they not used in practice?
	- They are too limited to be useful.

### Summary

- The HRU-revisited paper shows that „proofs“, even when rather old and frequently cited, might contain some bugs.
- The HRU-revisited paper shows that modeling the notion of security/safety is not trivial at all.
- HRU and HRU-revisited show how science evolves (using falsifiable statements).
