
# Secure Data Outsourcing & Access Pattern Confidentiality

## Secure Data Outsourcing

- pro:
	- cost
	- availability & scalability
	- administration
- con:
	- Confidentiality

> My sensitive corporate data will never be in the cloud.

### Inference attacks on static data


- Naïve approach:
	- encrypt all data probabilistic (basically 1 big zip-file)
		- problem: inefficient queries
	- encrypt all records on their own
		- Deterministic --> Attack with public background knowledge
		- order-preserving --> same
	-  static data in general
		- statistical attacks on access patters


## Access Pattern Confidentiality
### Attacker model and definition

- "honest-but-curious" Attacker
	- not really honest, and not only curious
	- can see encrypted data and traffic
	- can not change traffic or data
	- may know plaintext of some data
	- know access patters
	- example:
		- stolen backup
		- sniffed traffic
		-

### Oblivious RAM (ORAM)

- read more data than you need
- shuffle after reading
- add dummy elements
- all blocks have fixed


![](assets/markdown-img-paste-2018091519125866.png)

- mediator
	- interface to client
	- not observable
	- implements this (client gets plain text)
	- creates dummy elements in storage

### Burst ORAM

- more efficient version of ORAM
- partitions data

## PATCONFDB

- use ORAM for entire database service
- indices
	- with ORAM
	- B-Tree
	- --> multiple requests just to get index

![](assets/markdown-img-paste-20180915191958660.png)

- Issues with multiple indices
	- same values accessed via different paths --> better prediction for attack

## Summary

- Cryptographically enforced access control
- Access Pattern Confidentiality
	-	Encryption alone might not besufficient in many - outsourcing scenarios
- Oblivious RAM
	- Burst ORAM
	- The performance of proposed approaches improves - quickly
- PATCONFB: Access Pattern Confidentiality for - Databases
	- B-tree index is outsourced to multiple ORAM instances
	- Performance of equality selections is already - competitive
- In database scenarios with multiple indices, several new problems are introduced --> access pattern confidentiality gets - even more expensive
