
# DRM and Usage Control

- Digital Rights Management (DRM)
- Usage Control (UCON)
	- = DRM + AC
	- adds to AC
		- continuity of access enforcement
			- not only prio to access, but also during
		- mutability:
			- access changes attributes as side effacts

## Usage Control

> UCON is not a substitute for traditional access control, trust management, or DRM. Rather, UCON encompasses these three areas and goes beyond in its definition and scope.

@UCON04

- replaces the concept of ACM

![UCON_ABC model components:](assets/markdown-img-paste-20180913134827501.png)


### UCON_ABC (Authorizations, oBligations, and Conditions)

- introduced by @UCON04
- im wesentlich ABAC, aber
	- Attribute ändern sich durch Zugriffe
		- nicht alle Attribute sind mutable
	- zusätzliche Checks während des Zugriffes
		- _pre_-functions sind die "normalen" ABAC-functions
		- _on_-functions sind die "neuen" UCON-functions
		- _post_ gibt es auch, werden aber nicht weiter behandelt

- Components of UCON ABC models
	- Subjects (S) and Subject attributes (ATT(S))
		- Example: S=user, ATT(S)={user_group}
	- Objects (O) and Object attributes (ATT(O))
		- Example: O=file, ATT(O)={security_level}
	- Rights (R)
		- Example: R={read}
		- Right types: direct, delegation, administrative
	- Authorizations (A)
		- Functional predicates that have to be evaluated for usage decision
		- Types of authorizations: preA (before the requested right is exercised by the subject on object) and onA (while the right is exercised by s on o)
	- Obligations (B)
		- functional predicates that verify mandatory requirements a subject has to perform before or during a usage exercise
		- Types of obligations: preB and onB
	- Conditions (C)
		- Environmental or system-oriented decision factors
		- Types of conditions: preC and onC
		- Evaluation of conditions cannot result in any attribute update

- UCON_ABC family
	- Man kann updates an verschiedenen Stellen erlauben
	- Weil das eine Rolle spielt, unterscheidet man zwischen A,B und C (anstatt einfach nur zu sagen "A macht alles")

![the 16 basic ucon models](assets/markdown-img-paste-20180913140005355.png)
