
# relationship based access control

- Homework was @PGP97
- Slides are based on @REBAC16

- Vokabeln
	- Online Socia Network (OSN)
	- user-to-user relationship-based access control (UURAC)
		- ReBAC, that utilizes regular expression notation for such policy specification.
		- proposed in @REBAC16

## characteristics

- Policy Individualization
	- user create their own rules
	- user want to control access to resources, that are linked to them
		- only friends of friends can view my photo
		- only peoeple tagged in photo may share it
- user create their own relationships
- user policies for incoming action
	- e.g. user blocks violent content, or other users entirely

## UURAC

(konkrete implementierung nicht Prüfungsrelevant)

- Relationship Category
	- Multiple Relationship Types
	- Directional Relationship
	- U2U Relationship
	- no U2Resource Relationship
- Model Characteristics
	- Policy Individualization
	- User & Resource as a Target
	- Outgoing/Incoming Action Policy
- Relationship Composition
	- Relationship Depth: 0 to n
	- Relationship Composition: different path patterns

![uurac data model example](assets/markdown-img-paste-20180912110005212.png)

### Policy Conflict Resolution

- Problem: different user might have conflicing policies
- solutions:
	- disjunction of policies (1 permit need)
	- conjunction of policies (all permits need)
	- prioreticing (e.g. target user overrules)

- Policies operate on the user graph
	- usually on paths between 2 users
	- different relationships types (e.g. "friend" and "coworker")
- RegExp can run on paths
	- limitation: paths are evaluated separetely
	- fast, but DOS-posibility
	- low usability

Bonus: ReBAC is compatible with ABAC: you can add attributes to Nodes and Edges

## conclusio

it's neat
